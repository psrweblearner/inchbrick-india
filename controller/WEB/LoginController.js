const view =async (req,res)=>{
if(req.cookies.admin_auth){
    res.redirect('/dashboard');
}else{
    res.render('login',{layout:false});
}
}
const logout = async(req,res)=>{
    try {
        // req.user.tokens = req.user.tokens.filter((currElement)=>{
        //     return currElement.token !== req.token;
        // });
        // for all device logout 
        req.user.tokens=[];
        res.clearCookie("admin_auth");
        await req.user.save();
        res.redirect('/');
    } catch (error) {
        res.status(401).send(error);
    }
}
module.exports ={view,logout}