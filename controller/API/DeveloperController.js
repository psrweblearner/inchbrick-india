const Developer = require('../../models/Developer');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await Developer.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new Developer({
            "name":req.body.name,
            "description":req.body.description,
            "logo":req.body.logo,
            "code":await uniqueId('developer',10),
            "slug":await slugify(req.body.name),
        });
        data.save();
        res.status(200).json({status:true,message:"Developer Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        await Developer.findOneAndUpdate({code:req.body.code},{$set:{
                "name":req.body.name,
                "description":req.body.description,
                "logo":req.body.logo,
                "status":req.body.status,
                "slug":await slugify(req.body.name),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
       } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const DeveloperDelete = async(req,res)=>{
    try {
        const chkId =await Developer.find({code:req.body.code});
        if(chkId.length==1){
          await Developer.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,DeveloperDelete}