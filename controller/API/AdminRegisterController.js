const AdminUser = require('../../models/AdminUser');
const {slugify,uniqueId} = require('../../config/helper');
const bcrypt =require('bcryptjs');

const index =async (req,res)=>{
    try {
        const data = await AdminUser.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
         let password = await uniqueId(`${req.body.firstName}${req.body.lastName}`,15);
        if(req.body.password){
            password = req.body.password;
        }
        const data = await new AdminUser({
            "firstName":req.body.firstName,
            "lastName":req.body.lastName,
            "email":req.body.email,
            "mobile":req.body.mobile,
            "userId":await uniqueId(`${req.body.firstName}${req.body.lastName}${req.body.mobile}`,10),
            "slug":await slugify(`${req.body.firstName} ${req.body.lastName}`),
            "password":password,
            "role":req.body.role
        });
        const chkSlug  =await AdminUser.find({slug:data.slug});
       if(chkSlug.length>0){
        res.status(403).json({status:false,message:"User allready exists! Please try with another name"});
       }
        data.save();
        res.status(200).json({status:true,message:"Register Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await AdminUser.find({userId:req.body.userId});
        if(chkId.length==1){
            const data = await AdminUser.findOneAndUpdate({userId:req.body.userId},{$set:{
                "firstName":req.body.firstName,
                "lastName":req.body.lastName,
                "email":req.body.email,
                "mobile":req.body.mobile,
                "about":req.body.about,
                "dob":req.body.dob,
                "address":req.body.address,
                "designation":req.body.designation,
                "is_agent":req.body.is_agent,
                "license":req.body.license,
                "profile":req.body.profile,
                "role":req.body.role,
                "status":req.body.status,
                "is_verify":req.body.is_verify,
                "slug":await slugify(`${req.body.firstName} ${req.body.lastName}`),
            }});
            const chkSlug  =await AdminUser.find({slug:data.slug});
            if(chkSlug.length>0){
                res.status(403).json({status:false,message:"User allready exists! Please try with another name"});
            }
            res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await AdminUser.find({userId:req.body.userId});
        if(chkId.length==1){
          await AdminUser.findOneAndDelete({userId:req.body.userId});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}
const login =async(req,res)=>{
    const {email,password} =req.body;
    try {
        const data =await AdminUser.findOne({email:email});
      if(!data){
        res.status(400).json({status:false,message:"Please try to login with correct credentials!"});
      }
      const passCompare = await bcrypt.compare(password,data.password);
    if(!passCompare){
        res.status(400).json({status:false,message:"Please try to login with correct credentials!"});
      }
        const token = await data.generateAuthToken();
        res.cookie("admin_auth",token,{
            expires:new Date(Date.now() + 6000000),
            httpOnly:true,
            // secure:true
        });
       res.status(200).json({status:true,token});
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}
module.exports ={index,store,update,remove,login}