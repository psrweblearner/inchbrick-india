const User = require('../../models/User');
const {uniqueId} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await User.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new User({
            "fullName":req.body.fullName,
            "email":req.body.email,
            "mobile":req.body.mobile,
            "userId":await uniqueId(`${req.body.fullName}${req.body.mobile}`,10),
            "password":req.body.password
        });
        data.save();
        res.status(200).json({status:true,message:"Register Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await User.find({userId:req.body.userId});
        if(chkId.length==1){
            const data = await User.findOneAndUpdate({userId:req.body.userId},{$set:{
                "fullName":req.body.fullName,
                "email":req.body.email,
                "mobile":req.body.mobile,
                "profile":req.body.profile,
                "status":req.body.status,
                "is_verify":req.body.is_verify
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const UserDelete = async(req,res)=>{
    try {
        const chkId =await User.find({userId:req.body.userId});
        if(chkId.length==1){
          await User.findOneAndDelete({userId:req.body.userId});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,UserDelete}