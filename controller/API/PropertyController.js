const Properties = require('../../models/Properties');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await Properties.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new Properties({
            "title":req.body.title,
            "code":await uniqueId('Properties',10),
            "slug":await slugify(req.body.title),
        });
        data.save();
        res.status(200).json({status:true,message:"Properties Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
            const data = await Properties.findOneAndUpdate({code:req.body.code},{$set:{
                "title":req.body.title,
                "description":req.body.description,
                "shortDesc":req.body.shortDesc,
                "placeId":req.body.placeId,
                "is_approved":req.body.is_approved,
                "is_recommended":req.body.is_recommended,
                "is_featured":req.body.is_featured,
                "is_commercial":req.body.is_commercial,
                "is_verified":req.body.is_verified,
                "is_spotlight":req.body.is_spotlight,
                "parking":req.body.parking,
                "furnishedType":req.body.furnishedType,
                "is_rental":req.body.is_rental,
                "rentalType":req.body.rentalType,
                "countryCode":req.body.countryCode,
                "stateCode":req.body.stateCode,
                "cityCode":req.body.cityCode,
                "houseNo":req.body.houseNo,
                "addressLine1":req.body.addressLine1,
                "addressLine2":req.body.addressLine2,
                "postCode":req.body.postCode,
                "latitude":req.body.latitude,
                "longitude":req.body.longitude,
                "status":req.body.status,
                "developerCode":req.body.developerCode,
                "categoryCode":req.body.categoryCode,
                "propertyTypeCode":req.body.propertyTypeCode,
                "slug":await slugify(req.body.title),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
       
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await Properties.find({code:req.body.code});
        if(chkId.length==1){
          await Properties.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}