const Blog = require('../../models/Blog');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await Blog.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new Blog({
            "title":req.body.title,
            "description":req.body.description,
            "shortDesc":req.body.shortDesc,
            "is_featured":req.body.is_featured,
            "thumbnail":req.body.thumbnail,
            "thumbnailInner":req.body.thumbnailInner,
            "blogType":req.body.blogType,
            "status":req.body.status,
            "code":await uniqueId('Blog',10),
            "slug":await slugify(req.body.title),
        });
        data.save();
        res.status(200).json({status:true,message:"Blog Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
            const data = await Blog.findOneAndUpdate({code:req.body.code},{$set:{
                "title":req.body.title,
                "description":req.body.description,
                "shortDesc":req.body.shortDesc,
                "is_featured":req.body.is_featured,
                "thumbnail":req.body.thumbnail,
                "thumbnailInner":req.body.thumbnailInner,
                "blogType":req.body.blogType,
                "status":req.body.status,
                "slug":await slugify(req.body.title),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
       
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await Blog.find({code:req.body.code});
        if(chkId.length==1){
          await Blog.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}