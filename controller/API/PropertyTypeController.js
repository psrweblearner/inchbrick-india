const PropertyType = require('../../models/PropertyType');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await PropertyType.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new PropertyType({
            "title":req.body.title,
            "description":req.body.description,
            "thumbnail":req.body.thumbnail,
            "code":await uniqueId('PropertyType',10),
            "slug":await slugify(req.body.title),
        });
        data.save();
        res.status(200).json({status:true,message:"PropertyType Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await PropertyType.find({code:req.body.code});
        if(chkId.length==1){
            const data = await PropertyType.findOneAndUpdate({code:req.body.code},{$set:{
                "title":req.body.title,
                "description":req.body.description,
                "thumbnail":req.body.thumbnail,
                "slug":await slugify(req.body.title),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await PropertyType.find({code:req.body.code});
        if(chkId.length==1){
          await PropertyType.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}