const Cities = require('../../models/City');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await Cities.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new Cities({
            "name":req.body.name,
            "stateCode":req.body.stateCode,
            "code":await uniqueId('Cities',10),
            "slug":await slugify(req.body.name),
        });
        data.save();
        res.status(200).json({status:true,message:"Cities Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await Cities.find({code:req.body.code});
        if(chkId.length==1){
            const data = await Cities.findOneAndUpdate({code:req.body.code},{$set:{
                "name":req.body.name,
                "stateCode":req.body.stateCode,
                "slug":await slugify(req.body.name),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await Cities.find({code:req.body.code});
        if(chkId.length==1){
          await Cities.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}