const PropertyImage = require('../../models/PropertyImages');
const index =async (req,res)=>{
    try {
        const data = await PropertyImage.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new PropertyImage({
            "title":req.body.title,
            "thumbnails":req.body.thumbnails,
            "propertyCode":req.body.propertyCode
        });
        data.save();
        res.status(200).json({status:true,message:"PropertyImage Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
            const data = await PropertyImage.findOneAndUpdate({_id:req.body.id},{$set:{
                "title":req.body.title,
                "thumbnails":req.body.thumbnails,
                "propertyCode":req.body.propertyCode,
                "shortOrder":req.body.order
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
       
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const data = await PropertyImage.findOneAndDelete({_id:req.body.id});
                res.status(200).json({status:true,message:"Delete Successfully"});
        
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}