const PropertyAmenity = require('../../models/PropertyAmenity');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await PropertyAmenity.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new PropertyAmenity({
            "title":req.body.title,
            "amenityType":req.body.amenityType,
            "icon":req.body.icon,
            "code":await uniqueId('PropertyAmenity',10),
            "slug":await slugify(req.body.title),
        });
        data.save();
        res.status(200).json({status:true,message:"PropertyAmenity Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await PropertyAmenity.find({code:req.body.code});
        if(chkId.length==1){
            const data = await PropertyAmenity.findOneAndUpdate({code:req.body.code},{$set:{
                "title":req.body.title,
                "amenityType":req.body.amenityType,
                "icon":req.body.icon,
                "slug":await slugify(req.body.title),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await PropertyAmenity.find({code:req.body.code});
        if(chkId.length==1){
          await PropertyAmenity.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}