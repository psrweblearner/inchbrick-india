const PropertyCategory = require('../../models/PropertyCategory');
const {uniqueId,slugify} = require('../../config/helper');
const index =async (req,res)=>{
    try {
        const data = await PropertyCategory.find();
        res.status(200).json(data);
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}
const store =async (req,res)=>{
    try {
       const data = await new PropertyCategory({
            "title":req.body.title,
            "description":req.body.description,
            "thumbnail":req.body.thumbnail,
            "code":await uniqueId('PropertyCategory',10),
            "slug":await slugify(req.body.title),
        });
        data.save();
        res.status(200).json({status:true,message:"PropertyCategory Added Successfully"});
        
    } catch (err) {
        return res.status(500).json({error:"No Data Found!",message:err.message})
    }
}

const update =async(req,res)=>{
    try {
        const chkId =await PropertyCategory.find({code:req.body.code});
        if(chkId.length==1){
            const data = await PropertyCategory.findOneAndUpdate({code:req.body.code},{$set:{
                "title":req.body.title,
                "description":req.body.description,
                "thumbnail":req.body.thumbnail,
                "slug":await slugify(req.body.title),
            }});
           res.status(200).json({status:true,message:"Update Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

const remove = async(req,res)=>{
    try {
        const chkId =await PropertyCategory.find({code:req.body.code});
        if(chkId.length==1){
          await PropertyCategory.findOneAndDelete({code:req.body.code});
            res.status(200).json({status:true,message:"Delete Successfully"});
        }else{
            res.status(404).json({status:false,message:"No data found!"});
        }
       
   } catch (err) {
       return res.status(500).json({error:"No Data Found!",message:err.message})
   }
}

module.exports ={index,store,update,remove}