const mongoose =require('mongoose');
const dotenv = require('dotenv');
dotenv.config({path:'../config.env'});
const db = process.env.DATABASE;
mongoose.set('strictQuery', true);
mongoose.connect(db,{
    useNewUrlParser:true,
    useUnifiedTopology:true
}).then(()=>{
    console.log('connection successfully connected with mongoDB');
}).catch((err)=>{
    console.log('there is some error', err);
})