const slugify =async (title) => {
    let slug;
    slug = title.toLowerCase();
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return await slug;
}

const uniqueId =async (text,length)=>{
    result           = '';
    start =''
    characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    charactersLength =characters.length;
    textLenght =text.length;
    for ( i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    for(u=0;u<3;u++){
        start += text.charAt(Math.floor(Math.random() * textLenght));
    }
    return await start+"-"+result;
}
module.exports ={slugify,uniqueId}