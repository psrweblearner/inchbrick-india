const jwt = require('jsonwebtoken');
const AdminUser =require('../models/AdminUser');
const adminAuth =async (req,res,next)=>{
    try {
        const token = req.cookies.admin_auth;
        const verifyToken = await jwt.verify(token,process.env.JWT_SECRET);
        const user =await AdminUser.findOne({_id:verifyToken._id});
        req.token =token;
        req.user =user;
        next();
    } catch (error) {
        res.status(401).send(error);
    }
}
module.exports =adminAuth;