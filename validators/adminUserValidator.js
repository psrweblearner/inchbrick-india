const { body } = require('express-validator');
const { baseModelName } = require('../models/AdminUser');
const AdminUserValidate =(req,res,next)=>{
    body('firstName').isLength({ min: 5 }),
    body('lastName').isLength({ min: 5 }),
    body('email').isEmail(),
    body('mobile').isNumeric()
}
module.exports =AdminUserValidate;