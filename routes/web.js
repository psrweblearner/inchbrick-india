const express = require('express');
const router = express.Router();
// middleware
const adminAuth =require('../middleware/AdminAuth'); 
// all controller
const login = require('../controller/WEB/LoginController');
const dashboard = require('../controller/WEB/DashboardController');
const developer = require('../controller/WEB/DeveloperController');
// all route
router.get('/',login.view);
router.get('/logout',adminAuth,login.logout);
router.get('/dashboard',adminAuth,dashboard.view);
router.get('/developer',adminAuth,developer.view);
router.get('/developer/:code',adminAuth,developer.edit);
module.exports = router