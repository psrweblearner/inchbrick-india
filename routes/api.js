const express = require('express');
const router = express.Router();
// middleware
const adminAuth =require('../middleware/AdminAuth'); 
// controller middleware
const adminLogin = require('../controller/API/AdminRegisterController');
const userLogin = require('../controller/API/UserRegisterController');
const developer = require('../controller/API/DeveloperController');
const propertyType = require('../controller/API/PropertyTypeController');
const propertyCategory = require('../controller/API/PropertyCategoryController');
const propertyAmenity = require('../controller/API/PropertyAmenityController');
const properties = require('../controller/API/PropertyController');
const propertyImages = require('../controller/API/PropertyImagesController');
const country = require('../controller/API/CountryController');
const state = require('../controller/API/StateController');
const city = require('../controller/API/CityController');
const blog = require('../controller/API/BlogController');

// http://localhost:5000/api/admin-register (Methods [GET,POST,PUT,DELETE]);
router.get('/admin-register',adminLogin.index);
router.post('/admin-register',adminLogin.store);
router.put('/admin-register',adminLogin.update);
router.delete('/admin-register',adminLogin.remove);
router.post('/admin-login',adminLogin.login);

// http://localhost:5000/api/user-login (Methods [GET,POST,PUT,DELETE]);
router.get('/user-login',userLogin.index);
router.post('/user-login',userLogin.store);
router.put('/user-login',userLogin.update);
router.delete('/user-login',userLogin.UserDelete);

// http://localhost:5000/api/developer (Methods [GET,POST,PUT,DELETE]);
router.get('/developer',developer.index);
router.post('/developer',adminAuth,developer.store);
router.put('/developer',developer.update);
router.delete('/developer',adminAuth,developer.DeveloperDelete);

// http://localhost:5000/api/property-type (Methods [GET,POST,PUT,DELETE]);
router.get('/property-type',propertyType.index);
router.post('/property-type',propertyType.store);
router.put('/property-type',propertyType.update);
router.delete('/property-type',propertyType.remove);

// http://localhost:5000/api/property-category (Methods [GET,POST,PUT,DELETE]);
router.get('/property-category',propertyCategory.index);
router.post('/property-category',propertyCategory.store);
router.put('/property-category',propertyCategory.update);
router.delete('/property-category',propertyCategory.remove);

// http://localhost:5000/api/property-amenity (Methods [GET,POST,PUT,DELETE]);
router.get('/property-amenity',propertyAmenity.index);
router.post('/property-amenity',propertyAmenity.store);
router.put('/property-amenity',propertyAmenity.update);
router.delete('/property-amenity',propertyAmenity.remove);

// http://localhost:5000/api/properties (Methods [GET,POST,PUT,DELETE]);
router.get('/properties',properties.index);
router.post('/properties',properties.store);
router.put('/properties',properties.update);
router.delete('/properties',properties.remove);

// http://localhost:5000/api/property-images (Methods [GET,POST,PUT,DELETE]);
router.get('/property-images',propertyImages.index);
router.post('/property-images',propertyImages.store);
router.put('/property-images',propertyImages.update);
router.delete('/property-images',propertyImages.remove);


// http://localhost:5000/api/country (Methods [GET,POST,PUT,DELETE]);
router.get('/country',country.index);
router.post('/country',country.store);
router.put('/country',country.update);
router.delete('/country',country.remove);


// http://localhost:5000/api/state (Methods [GET,POST,PUT,DELETE]);
router.get('/state',state.index);
router.post('/state',state.store);
router.put('/state',state.update);
router.delete('/state',state.remove);

// http://localhost:5000/api/city (Methods [GET,POST,PUT,DELETE]);
router.get('/city',city.index);
router.post('/city',city.store);
router.put('/city',city.update);
router.delete('/city',city.remove);

// http://localhost:5000/api/blog (Methods [GET,POST,PUT,DELETE]);
router.get('/blog',blog.index);
router.post('/blog',blog.store);
router.put('/blog',blog.update);
router.delete('/blog',blog.remove);

module.exports = router