const mongoose = require('mongoose');
const {Schema} =mongoose;
const PropertyAmenitySchema = new Schema({
    title:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    amenityType:{
        type:String,
    },
    icon:{
        type:String,
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const PropertyAmenity = mongoose.model('propertyAmenity',PropertyAmenitySchema);
PropertyAmenity.createIndexes();
module.exports = PropertyAmenity;
