const mongoose = require('mongoose');
const bcrypt =require('bcryptjs');
const jwt_secret =process.env.JWT_SECRET;
var jwt = require('jsonwebtoken');
const {Schema} =mongoose;
const AdminUserSchema = new Schema({
    firstName:{
        type:String,
        required:true
    },
    lastName:{
        type:String,
        required:true
    },
    userId:{
        type:String,
        unique:true,
        required:true
    },
    role:{
        type:Number
    },
    slug:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    mobile:{
        type:String,
        required:true
    },
    about:{
        type:String
    },
    dob:{
        type:Date
    },
    address:{
        type:String
    },
    designation:{
        type:String
    },
    profile:{
        type:String
    },
    is_agent:{
        type:Number,
        default:0
    },
    license:{
        type:String
    },
    status:{
        type:Number,
        default:0
    },
    is_verify:{
        type:Boolean,
        default:false
    },
    tokens:[{
        token:{
            type:String,
            required:true
        }
    }],
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})
AdminUserSchema.methods.generateAuthToken = async function(){
    try {
        const token = jwt.sign({_id:this._id.toString()},jwt_secret);
        this.tokens = this.tokens.concat({token});
        await this.save();
        return token;
    } catch (error) {
        res.status(400).json({status:false,message:"Token not generated!"});
    }
}

AdminUserSchema.pre('save',async function(next){
    if(this.isModified('password')){
        this.password =await bcrypt.hash(this.password,12);
    }
    next();
})
const AdminUser = mongoose.model('adminUser',AdminUserSchema);
AdminUser.createIndexes();
module.exports = AdminUser;
