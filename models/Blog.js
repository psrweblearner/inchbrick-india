const mongoose = require('mongoose');
const {Schema} =mongoose;

const BlogSchema = new Schema({
    title:{
        type:String,
        required:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    thumbnail:{
        type:String
    },
    thumbnailInner:{
        type:String
    },
    blogType:{
        type:String,
        default:"BLOG"
    },
    shortDesc:{
        type:String
    },
    description:{
        type:String,
    },
    slug:{
        type:String,
        unique:true
    },
    is_featured:{
        type:Number,
        default:0
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const Blog = mongoose.model('blog',BlogSchema);
Blog.createIndexes();
module.exports = Blog;
