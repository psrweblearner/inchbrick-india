const mongoose = require('mongoose');
const {Schema} =mongoose;
const DeveloperSchema = new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    description:{
        type:String,
    },
    logo:{
        type:String,
    },
    slug:{
        type:String,
        unique:true
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const Developer = mongoose.model('developer',DeveloperSchema);
Developer.createIndexes();
module.exports = Developer;
