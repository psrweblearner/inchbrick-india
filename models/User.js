const mongoose = require('mongoose');
const bcrypt =require('bcryptjs');
const {Schema} =mongoose;
const UserSchema = new Schema({
    fullName:{
        type:String,
        required:true
    },
    userId:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        required:true
    },
    mobile:{
        type:String,
        unique:true,
        required:true
    },
    profile:{
        type:String
    },
    status:{
        type:Number,
        default:0
    },
    is_verify:{
        type:Boolean,
        default:false
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})
UserSchema.pre('save',async function(next){
    if(this.isModified('password')){
        this.password =await bcrypt.hash(this.password,12);
    }
    next();
})
const User = mongoose.model('user',UserSchema);
User.createIndexes();
module.exports = User;
