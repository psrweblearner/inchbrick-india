const mongoose = require('mongoose');
const {Schema} =mongoose;
const StateSchema = new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    countryCode:{
        type:String
    },
    slug:{
        type:String,
        unique:true
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const State = mongoose.model('state',StateSchema);
State.createIndexes();
module.exports = State;
