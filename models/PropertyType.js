const mongoose = require('mongoose');
const {Schema} =mongoose;
const PropertyTypeSchema = new Schema({
    title:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    description:{
        type:String,
    },
    thumbnail:{
        type:String,
    },
    slug:{
        type:String,
        unique:true
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const PropertyType = mongoose.model('propertyType',PropertyTypeSchema);
PropertyType.createIndexes();
module.exports = PropertyType;
