const mongoose = require('mongoose');
const {Schema} =mongoose;
const PropertyImageSchema = new Schema({
    propertyCode:{
        type:String
    },
    title:{
        type:String,
        required:true,
    },
    thumbnails:{
        type:String,
    },
    shortOrder:{
        type:Number,
        default:0
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const PropertyImage = mongoose.model('PropertyImage',PropertyImageSchema);
PropertyImage.createIndexes();
module.exports = PropertyImage;
