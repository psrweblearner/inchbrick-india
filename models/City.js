const mongoose = require('mongoose');
const {Schema} =mongoose;
const CitiesSchema = new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    stateCode:{
        type:String
    },
    slug:{
        type:String,
        unique:true
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const Cities = mongoose.model('cities',CitiesSchema);
Cities.createIndexes();
module.exports = Cities;
