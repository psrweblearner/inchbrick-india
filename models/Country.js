const mongoose = require('mongoose');
const {Schema} =mongoose;
const CountrySchema = new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
    },
    code:{
        type:String,
        unique:true,
        required:true
    },
    slug:{
        type:String,
        unique:true
    },
    status:{
        type:Number,
        default:0
    },
    created_at:{
        type:Date,
        default:Date.now
    },
    update_at:{
        type:Date,
        default:Date.now
    }
})

const Country = mongoose.model('countries',CountrySchema);
Country.createIndexes();
module.exports = Country;
