const express = require('express');
const app =express();
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');
dotenv.config({path:'./config.env'});
require('./config/db');
const bodyParser = require('body-parser');
const path =require('path');
const cors = require('cors');
// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// hbs layout integration
const exphbs = require('express-handlebars');
const hbs =require("hbs");
const PORT = process.env.PORT || 5000
app.use(cors());
// express static path
const staticpath = path.join(__dirname,"./public");
const template_path = path.join(__dirname,"./templates/views");

// bootstrap and jquery
app.use('/css',express.static(path.join(__dirname,"./node_modules/bootstrap/dist/css")));
app.use('/js',express.static(path.join(__dirname,"./node_modules/bootstrap/dist/js")));
app.use('/jq',express.static(path.join(__dirname,"./node_modules/jquery/dist")));
app.use('/summernote',express.static(path.join(__dirname,"./node_modules/summernote/dist")));

app.use(express.static(staticpath));
app.use(cookieParser());
app.engine('.hbs', exphbs.engine({ extname: '.hbs', defaultLayout: "main",
helpers: {
    section: function(name, options) { 
      if (!this._sections) this._sections = {};
        this._sections[name] = options.fn(this); 
        return null;
      }
  }    }));
app.set("view engine","hbs");
app.set("views",template_path);
// Configuring for route
app.use('/api',require('./routes/api'))
app.use('/',require('./routes/web'))
app.listen(PORT,()=>{
    console.log(`Example app listening at http://localhost:${PORT}`);
})